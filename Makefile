up:
	@docker-compose up -d
down:
	@docker-compose down
ps:
	docker ps --format="table {{.ID}}\t{{.Names}}\t{{.Image}}\t{{.Status}}\t{{.Ports}}"
back:
	docker exec -it a9a04ba145db bash
php:
	docker exec -it php-ms bash
right:
	sudo chown -R cuttieflame /home/cuttieflame/projects/lara/larams
storage:
	chmod -R 777 storage
storage-error:
	chmod -R gu+w storage
seed:
	make php && php artisan migrate:refresh --seed
trunlog:
	echo "" > storage/logs/laravel.log
log:
	tail -f storage/logs/laravel.log
cllog:
	:> storage/logs/laravel.log
# chmod -R 777 storage/
# sudo chmod -R 777 bootstrap/cache/
