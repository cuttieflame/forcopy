<?php

use App\Http\Controllers\TestController;
use Illuminate\Support\Facades\Route;
use Laravel\Socialite\Facades\Socialite;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/

Route::get('/test', function () {
    return Socialite::driver('google')->redirect();
//    \App\Services\SendService::send(['123']);
//    \App\Jobs\TestJob::dispatch();
});
Route::get('/test', [TestController::class, 'test'])->middleware('json.response');
