<?php declare(strict_types=1);

namespace App\DTO;

use Illuminate\Http\Request;

readonly class RegisterAuthRequestDTO
{
    public function __construct(
        public string $name,
        public string $email,
        public string $password,
    )
    {
    }

    public static function fromRequest(Request $request): static
    {
        return new static(
            name: $request->input('name'),
            email: $request->input('email'),
            password: $request->input('password'),
        );
    }
}
