<?php declare(strict_types=1);

namespace App\DTO;

use Illuminate\Http\Request;

readonly class LoginAuthRequestDTO
{
    public function __construct(
        public string $email,
        public string $password,
    )
    {
    }

    public static function fromRequest(Request $request): static
    {
        return new static(
            email: $request->input('email'),
            password: $request->input('password'),
        );
    }
}
