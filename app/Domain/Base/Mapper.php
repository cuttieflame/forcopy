<?php

namespace App\Domain\Base;

abstract class Mapper
{
    abstract public static function makeDTO(BaseEntity $model, $parent = null) : DomainModel;
}
