<?php

namespace App\Domain\Base;

use Illuminate\Database\Eloquent\Model;

abstract class EloquentRepository
{
    abstract public function getEntity(): Model;

    abstract public function getMapper(): Mapper;

    protected function create(Model $model): DomainModel
    {
        return new DomainModel();
    }
}
