<?php

namespace App\Domain\Base;

abstract class Struct
{
    /* constructor, takes in data */
    function __construct($mixed = array())
    {
        $arr = [];

        if (!is_array($mixed) || func_num_args() > 1) {
            $mixed = func_get_args();
        }

        if ($this->_isAssoc($mixed)) {
            foreach ($mixed as $prop => $val) {
                if (!is_numeric($prop)) {
                    $this->$prop = $val;
                }
            }
        } else {
            foreach ($mixed as $prop) {
                if (!is_numeric($prop)) {
                    $this->$prop = false;
                }
            }
        }
    }

    /* utility function to decide if an array is associative */
    protected function _isAssoc($var)
    {
        return is_array($var) && array_diff_key($var, array_keys(array_keys($var)));
    }

    /* implementation of __set, just chucks stuff in _data */
    function __set($name, $value)
    {
        $this->$name = $value;
    }

    /* grabs stuff from _data */
    function __get($name)
    {
        if (isset($this->$name)) {
            return $this->$name;
        }
        return false;
    }

//    function rewind() {
//        reset($this->_data);
//    }
//
//    function current() {
//        return current($this->_data);
//    }
//
//    function key() {
//        return key($this->_data);
//    }
//
//    function next() {
//        return next($this->_data);
//    }
//
//    function valid() {
//        return ($this->current() !== false);
//    }
}
