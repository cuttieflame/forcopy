<?php declare(strict_types=1);

namespace App\Domain\Entities;

use App\Domain\Base\BaseEntity;
use Illuminate\Database\Eloquent\Model;

/**
 * @property int $id
 */
class UserEntity extends Model implements BaseEntity
{
    protected $table = 'users';

    protected $fillable = [
        'name',
        'email',
        'password',
    ];

    protected $hidden = [
        'password',
        'remember_token',
    ];

    protected $casts = [
        'email_verified_at' => 'datetime',
        'password' => 'hashed',
    ];
}
