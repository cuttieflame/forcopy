<?php

namespace App\Domain\Mapper;

use App\Domain\Base\BaseEntity;
use App\Domain\Base\DomainModel;
use App\Domain\Base\Mapper;
use App\Domain\Entities\UserEntity;
use App\Domain\Models\User;

class UserMapper extends Mapper
{
    /**
     * @param UserEntity $model
     * @param null $parent
     * @return DomainModel
     */
    public static function makeDTO(BaseEntity $model, $parent = null): \App\Domain\Base\DomainModel
    {
        $user = new User([
            'id' => $model->id,
            'name' => $model->name,
            'email' => $model->email
        ]);

        return $user;
    }
}
