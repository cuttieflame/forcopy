<?php

namespace App\Domain\Repository;

use App\Domain\Base\DomainModel;
use App\Domain\Base\EloquentRepository;
use App\Domain\Base\Mapper;
use App\Domain\Entities\UserEntity;
use App\Domain\Mapper\UserMapper;
use Illuminate\Database\Eloquent\Model;

class UserRepository extends EloquentRepository
{
    public function getEntity(): Model
    {
        return new UserEntity();
    }

    public function getMapper(): Mapper
    {
        return new UserMapper();
    }

    public function first(): DomainModel
    {
        $user = $this->getEntity()::query()
            ->first();

        return UserMapper::makeDTO($user);
    }
}
