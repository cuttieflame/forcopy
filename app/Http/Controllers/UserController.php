<?php declare(strict_types=1);

namespace App\Http\Controllers;

use App\DTO\LoginAuthRequestDTO;
use App\DTO\RegisterAuthRequestDTO;
use App\Http\Requests\LoginAuthRequest;
use App\Http\Requests\RegisterAuthRequest;
use App\Http\Resources\UserResource;
use App\Http\Responses\ApiErrorResponse;
use App\Http\Responses\ApiSuccessResponse;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Symfony\Component\HttpFoundation\Response as ResponseAlias;

class UserController extends Controller
{
    public function user(Request $request)
    {
        dd($request->user());
    }

    /**
     * @param LoginAuthRequest $request
     * @return ApiSuccessResponse|ApiErrorResponse
     */
    public function login(LoginAuthRequest $request): ApiSuccessResponse|ApiErrorResponse
    {
        $loginAuthDTO = LoginAuthRequestDTO::fromRequest($request);
        $loginCredentials = [
            'email' => $loginAuthDTO->email,
            'password' => $loginAuthDTO->password,
        ];

        if (!Auth::attempt($loginCredentials)) {
            return new ApiErrorResponse('UnAuthorised Access', null, ResponseAlias::HTTP_UNAUTHORIZED);
        }
        /* @var User $user */
        $user = Auth::user();
        $userLoginToken = $user->createToken(md5($loginAuthDTO->email))->accessToken;

        return new ApiSuccessResponse('',
            ['token_type' => 'Bearer', 'token' => $userLoginToken],
            ResponseAlias::HTTP_OK);
    }

    /**
     * @param RegisterAuthRequest $request
     * @return ApiSuccessResponse
     */
    public function register(RegisterAuthRequest $request): ApiSuccessResponse
    {
        $registerAuthDTO = RegisterAuthRequestDTO::fromRequest($request);

        /* @var User $user */
        $user = User::query()->create([
            'name' => $registerAuthDTO->name,
            'email' => $registerAuthDTO->email,
            'password' => bcrypt($registerAuthDTO->password)
        ]);

        $accessTokenExample = $user->createToken(md5($registerAuthDTO->email));
        return new ApiSuccessResponse(
            UserResource::make($user),
            ['token' => $accessTokenExample],
            ResponseAlias::HTTP_OK
        );
    }
}
