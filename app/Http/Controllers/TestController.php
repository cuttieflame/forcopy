<?php

namespace App\Http\Controllers;

use App\Domain\Repository\UserRepository;
use App\Http\Resources\UserResource;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

final class TestController extends Controller
{
    public function __construct(
        protected UserRepository $userRepository
    )
    {

    }

    public function test(Request $request)
    {
        $user = $this->userRepository->first();

        return response()->json([
            'data'=>UserResource::make($user),
        ]);
    }
}
